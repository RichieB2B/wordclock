RED_VALUE = 0
GREEN_VALUE = 120
BLUE_VALUE = 0


function time_to_leds()
	local current_hour, minute_value, minute_value_rounded
	local hour_word = {"een","twee","drie","vier", "vijf","zes","zeven","acht","negen","tien","elf","twaalf"}
	local wordleds = {
		acht = {45,46,47,48},
		twee = {48,49,50,51},
		zes = {52,53,54},
		drie = {44,43,42,41},
		elf = {41,40,39},
		tien = {38,37,36,35},
		zeven = {26,27,28,29,30},
		negen = {30,31,32,33,34},
		vier = {24,23,22,21},
		twaalf = {20,19,18,17,16,15},
		een = {5,6,7},
		vijf = {8,9,10,11},
	}
	
	local unix, micros
	unix,micros = rtctime.get()
	if math.floor((unix / 3600) + TIMEZONE) % 24 == 3 and tmr.time() > 7200 then
		node.restart()
	end
	current_hour = hour_word[math.floor((unix / 3600) + TIMEZONE) % 12]
	if current_hour == nil then
		current_hour = hour_word[12]
	end
	next_hour = hour_word[math.floor(((unix / 3600) + TIMEZONE) % 12)+1]
	minute_value = (math.floor(unix/60) % 60) % 5
	minute_value_rounded = math.floor((unix/60 % 60) / 5)
	

	local result 
	if     minute_value_rounded ==  0 then
		result = {85,86,87, 90,91, 12,13,14}
		for k,v in pairs(wordleds[current_hour]) do table.insert(result,v) end
	elseif minute_value_rounded ==  1 then
		result = {85,86,87, 90,91, 83,82,81,80, 71,72,73,74}
		for k,v in pairs(wordleds[current_hour]) do table.insert(result,v) end
	elseif minute_value_rounded ==  2 then
		result = {85,86,87, 90,91,  79,78,77,76, 71,72,73,74}
		for k,v in pairs(wordleds[current_hour]) do table.insert(result,v) end
	elseif minute_value_rounded ==  3 then
		result = {85,86,87, 90,91, 65,66,67,68,69, 71,72,73,74}
		for k,v in pairs(wordleds[current_hour]) do table.insert(result,v) end
	elseif minute_value_rounded ==  4 then
		result = {85,86,87, 90,91,  79,78,77,76, 64,63,62,61, 59,58,57,56}
		for k,v in pairs(wordleds[next_hour]) do table.insert(result,v) end
	elseif minute_value_rounded ==  5 then
		result = {85,86,87, 90,91,  83,82,81,80, 64,63,62,61, 59,58,57,56}
		for k,v in pairs(wordleds[next_hour]) do table.insert(result,v) end
	elseif minute_value_rounded ==  6 then
		result = {85,86,87, 90,91, 59,58,57,56}
		for k,v in pairs(wordleds[next_hour]) do table.insert(result,v) end
	elseif minute_value_rounded ==  7 then
		result = {85,86,87, 90,91,  83,82,81,80, 71,72,73,74, 59,58,57,56}
		for k,v in pairs(wordleds[next_hour]) do table.insert(result,v) end
	elseif minute_value_rounded ==  8 then
		result = {85,86,87, 90,91,  79,78,77,76, 71,72,73,74, 59,58,57,56}
		for k,v in pairs(wordleds[next_hour]) do table.insert(result,v) end
	elseif minute_value_rounded ==  9 then
		result = {85,86,87, 90,91, 65,66,67,68,69, 64,63,62,61}
		for k,v in pairs(wordleds[next_hour]) do table.insert(result,v) end
	elseif minute_value_rounded == 10 then
		result = {85,86,87, 90,91, 79,78,77,76, 64,63,62,61}
		for k,v in pairs(wordleds[next_hour]) do table.insert(result,v) end
	elseif minute_value_rounded == 11 then
		result = {85,86,87, 90,91, 83,82,81,80, 64,63,62,61}
		for k,v in pairs(wordleds[next_hour]) do table.insert(result,v) end
	end

	local minutes = {}
	if minute_value == 1 then
		table.insert(result,4)
	elseif minute_value == 2 then
		table.insert(result,4)
		table.insert(result,3)
	elseif minute_value == 3 then
		table.insert(result,4)
		table.insert(result,3)
		table.insert(result,2)
	elseif minute_value == 4 then
		table.insert(result,4)
		table.insert(result,3)
		table.insert(result,2)
		table.insert(result,1)
	end

	bytes = {}
	for i=1,3*99 do
		bytes[i]=0
	end
	for i,k in pairs(result) do
		-- print(k)
		bytes[3*k-2] = GREEN_VALUE
		bytes[3*k-1] = RED_VALUE
		bytes[3*k] = BLUE_VALUE
	end
	local rgb = string.char(unpack(bytes))
	ws2812.write(rgb)
end





