-- helper to extract first of variable number of arguments
function fst(...) 
	local f
	f,_ = ...
	return f
end


-- function colorscale(v)
-- 	-- return math.sqrt(v)*16
-- 	-- return v*v/255
-- 	return v^1.5/16
-- end